## Inleiding

`risdhr` staat voor `R`aads`I`nformatie`S`ysteem `D`en `H`aag en de laatste `R` staat voor de programmeertaal R. Dit package kan je helpen de metadata én tekst uit documenten van het RIS te downloaden, in te zoeken en de tekst te exporteren.

## Het RIS is als data bijgevoegd!

Voordat je aan de slag gaat met het downloaden van het complete RIS: alle metadata van het eerste document tot en met het laatste document van de laatste volledige maand is al bij dit package bijgevoegd! De dataset is - zodra je het package geladen hebt - beschikbaar onder de objectnaam `ris_data`.

Het updaten van de data wordt handmatig een keer per maand doorgevoerd. Hier kan - afhankelijk van weekenden en vakanties - enige vertraging in zitten.

Je kunt een beschrijving van de dataset vinden door `?ris_data` in de terminal te typen.

In deze package wordt een object met metadata een `ris_object` genoemd.

## Hoe gebruik je het package?

### Zoeken en analyseren van het RIS

#### In de tekstvariabelen zoeken met `ris_tekst_zoeken()`

Misschien wil je de dataset filteren op de aanwezigheid van een bepaald woord of een combinatie van woorden? Dat kan met de functie `ris_tekst_zoeken`.

Deze functie kent 3 argumenten:

1.  het `ris_object` waarin gezocht moet worden
2.  de `zoekterm` waar naar gezocht moet worden. Dit is een *reguliere expressie.*
3.  of de zoekopdracht `hoofdlettergevoelig` is. Bij `TRUE` wordt er wél met hoofdletters rekening gehouden, bij `false` niet.

De functie zoekt in alle tekstvelden in het `ris_object`. Op het moment dat je de tekst uit de PDF-bestanden dus aan het `ris_object` hebt toegevoegd met `ris_tekst_toevoegen()`, zoekt de functie ook in de variabele `tekst`.

Hieronder zoek ik naar de aanwezigheid van het woord `vreugdevuren` in de dataset `ris_data`:

    vreugdevuren <- ris_tekst_zoeken(ris_data, 'vreugdevuren', hoofdlettergevoelig = FALSE)

Gedetailleerde informatie over het gebruik vind je door in de terminal `?ris_tekst_zoeken()` te typen.

#### Toevoegen van tekst uit PDF-bestanden met `ris_tekst_toevegen()`

De package biedt de mogelijkheid om de tekst uit PDF-bestanden te downloaden en aan de dataframe toe te voegen. Dit gebeurt met de functie `ris_tekst_toevoegen`. Wanneer deze functie wordt toegepast wordt er een variabele `tekst` aan het `ris_object` toegevoegd.

Hieronder download ik de tekst van alle documenten in het `ris_object` `vreugdevuren`:

    vreugdevuren_tekst <- vreugdevuren %>% 
      ris_tekst_toevoegen()

Gedetailleerde informatie over het gebruik vind je door in de terminal `?ris_tekst_toevegen()` te typen.

Let bij het gebruik van deze functie wel op: het beschikbare werkgeheugen van je computer is de beperkende factor bij het downloaden van tekst uit PDF-bestanden. Ik raad het dan ook zéér af om de tekst van duizenden bestanden proberen toe te voegen aan je `ris_object`. Bij een gemiddelde computer moet enkele tientallen tot enkele honderdtallen - afhankelijk van de hoeveelheid werkgeheugen - wel lukken.

#### Toevoegen van verwijzingen naar andere RIS-documenten met `ris_verwijzingen_toevoegen()`

De package biedt de mogelijkheid om in de inhoud van documenten te zoeken naar verwijzingen naar andere RIS-documenten. Hiervoor wordt gezocht naar de combinatie van de letters `RIS` met een reeks cijfers.

Met de functie `ris_verwijzingen_toevoegen()` worden deze in een nieuwe kolom `risnummers`.

Hieronder voeg ik de verwijzingen toe aan het object `vreugdevuren_tekst`:

    vreugdevuren_verwijzingen <- vreugdevuren_tekst %>%
      ris_verwijzingen_toevoegen

#### Netwerk tussen documenten bekijken met `ris_netwerk_maken()`

De package biedt de mogelijkheid om de relaties tussen documenten zichtbaar te maken met `ris_netwerk_maken()`. Deze functie maakt een interactief netwerk, waarin:

-   elk document als cirkel wordt weergegeven. De (relatieve) grootte van de cirkel geeft aan hoe vaak er van of naar een document is verwezen.
-   elke verwijzing naar andere documenten wordt weergegeven door een verbindingslijn met een pijl tussen de cirkels (documenten). De realitevie dikte van de verbindingslijn geeft aan hoe vaak er vanuit het ene document naar het andere document wordt verwezen.
-   de kleur van de cirkel aangeeft uit welke module het document afkomstig is: *raadsvragen*, *moties* of *overige bestuurlijke stukken*).

Hieronder maak ik een netwerk van de documenten uit `vreugdevuren_verwijzingen` en geef deze vervolgens weer:

```{netwerk <- vreugdevuren_verwijzingen %>%}
  ris_netwerk_maken()

netwerk
```

#### Netwerk tussen documenten exporteren met `ris_netwerk_dashboard()`

De package biedt de mogelijkheid om de relaties tussen documenten zichtbaar te maken en deze vervolgens te exporteren naar een HTML-dashboard met `ris_netwerk_dashboard()`. Deze functie maakt eenzelfde interactief netwerk als met `ris_netwerk_maken()`, maar exporteert deze naar een webpagina die je op elk later moment kunt openen, óók zonder `Rstudio`.

Deze functie kent 3 argumenten:

1.  het `ris_object` waarvan een dashboard gemaakt moet worden;
2.  de `titel` van het dashboard. Dit wordt bovenaan het dashboard weergegeven*.* Wanneer geen waarde wordt opgegeven krijgt het dashboard de titel `RIS-netwerk`.
3.  de `bestandsnaam` van het dashboard. Dit moet eindigen op `.html`. Wanneer geen waarde wordt opgegeven krijgt het dashboard de bestandsnaam `dashboard.html`.

Het dashboard wordt opgeslagen in de map `dashboard`.

Hieronder maak ik een dashboard van de documenten uit

    ris_netwerk_dashboard(vreugdevuren_verwijzingen, titel = 'Vreugdevuren', bestandsnaam = 'vreugdevuren.html'

### Exporteren van data en bestanden

#### PDF-bestanden exporteren met `ris_pdf_opslaan()`

De package biedt de mogelijkheid om de PDF-bestanden te downloaden en in een opgegeven map op te slaan. Dit gebeurt met de functie `ris_pdf_opslaan`. Deze functie kent 2 argumenten:

1.  het `ris_object` waar de url's in staan van de te downloaden PDF-bestanden.
2.  de `folder` waar de PDF-bestanden opgeslagen moeten worden. Standaard is dit de map `PDF_export` (die wordt aangemaakt als deze nog niet bestaat). Je kunt deze naar wens veranderen.

Hieronder download ik de PDF-bestanden van alle documenten in het `ris_object` `vreugdevuren`:

    vreugdevuren %>% 
      ris_pdf_opslaan()

Gedetailleerde informatie over het gebruik vind je door in de terminal `?ris_pdf_opslaan` te typen.

#### Tekst exporteren met `ris_tekst_exporteren()`

Misschien wil je de tekst uit de variabele `tekst` exporteren naar tekstbestanden, bijvoorbeeld voor analyse in een kwantitatief analyseprogramma. Dit kan met de functie `ris_tekst_exporteren()`. Deze functie kent 2 argumenten:

1.  het `ris_object` waar het tekstveld in staat dat geëxporteerd moet worden.
2.  de `folder` waar de tekst naar geëxporteerd moet worden. Standaard is dit de map `Tekst_export` (die wordt aangemaakt als deze nog niet bestaat). Je kunt deze naar wens veranderen.

Hieronder exporteer ik de tekst uit `vreugdevuren` waarin de variabele `tekst` aanwezig is naar de standaard map `Tekst_export`:

    vreugdevuren %>% 
      ris_tekst_exporteren()

De bestanden zijn nu te vinden in de map `Tekst_export` en te lezen met een tekst-editor, of gewoon in Rstudio.

#### `ris_object` exporteren met `ris_export_csv()`

Misschien wil je het resultaat van je zoekopdracht exporteren naar een CSV-bestand, om hem bijvoorbeeld in Excel te openen. Dit kan met de functie `ris_export_csv()`. Deze functie kent drie argument:

1.  het `ris_object` dat naar een CSV-bestand geëxporteerd moet worden;
2.  de `bestandsnaam` waarmee het bestand moet worden opgeslagen. Dit móet eindigen op `.csv`. Standaard is dit de bestandsnaam `ris_object.csv`.
3.  de `folder` waar het CSV-bestand opgeslagen moet worden. Standaard is dit de map `Data_export` (die wordt aangemaakt als deze nog niet bestaat). Je kunt deze naar wens veranderen.

Hieronder exporteer ik het `ris_object` `vreugdevuren` naar een CSV-bestand:

    vreugdevuren %>% 
      ris_export_csv()

### Downloaden van metadata met `ris_get()` en `ris_get_all()`

In principe is `ris_data` bijgewerkt tot de laatste maand. Maar misschien wil je ook de meta-data van de lopende maand hebben. Óf heb je het gevoel dat het RIS is veranderd en wil je van een specifieke periode de meta-data opnieuw downloaden.

Dit alles kan met de functies `ris_get()` en `ris_get_all()`

#### `ris_get()`

De functie `ris_get()` kent 3 argumenten:

1.  het `jaar` of de `jaren` waarvan de metadata gedownload moeten worden;
2.  de `maand` of de `maanden` waarvan de metadata gedownload moeten worden;
3.  de `module` of de `modules` waarvan de metadata gedownload moet worden.

Hieronder download ik de metadata van het RIS uit het jaar `2021` uit maand `3` (maart) en uit de modules `raadsvragen` en `moties`.

    test <- ris_get(2021, 3, c('raadsvragen', 'moties'))

Gedetailleerde informatie over het gebruik vind je door in de terminal `?ris_get` te typen.

Wil je de gedownloade metadata toevoegen aan de in de package aanwezige metadata? Dat kun je doen met de functie `bind_rows` uit de package `dplyr`:

    update_ris <- ris_data %>% 
      dplyr::bind_rows(test)

#### `ris_get_all()`

De functie `ris_get_all()` is hetzelfde als de functie `ris_get()`, maar download de metadata van alle drie de modules. Daarmee kent deze functie dus 2 argumenten:

1.  het `jaar` of de `jaren` waarvan de metadata gedownload moeten worden;
2.  de `maand` of de `maanden` waarvan de metadata gedownload moeten worden.
