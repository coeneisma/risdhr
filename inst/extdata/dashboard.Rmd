---
output:
  flexdashboard::flex_dashboard:
    orientation: columns
    vertical_layout: fill
params:
  titel:
    value: "`r titel`"
    label: "Titel"
  aantal_edges:
    value: "`r aantal_edges`"
  aantal_nodes:
    value: "`r aantal_nodes`"
title: "`r params$titel`"
---

```{r setup, include=FALSE}
library(flexdashboard)
library(tidyverse)
library(RisdhR)
library(reactable)
library(glue)
```

# Netwerk

## Column {data-width="650"}

### Netwerk

```{r netwerk_weergeven}
netwerk
```

## Column {data-width="350"}

### Algemene informatie.

Dit is het dashboard *`r titel`*.

Je kunt linksboven het netwerk filteren op een *risnummer* (`Select by id`) of op *type document* (`Select by group`).

Je kunt de knopen slepen om ze te herordenen en inzoomen met je muis. Als je op een knoop met je muis blijft stilstaan krijg je de volgende informatie te zien:

-   Het `RIS-nummer`. Dit is een klikbare link, die je naar de betreffende pagina op het raadsinformatiesysteem leidt;
-   De `Titel` van de pagina op het raadsinformatiesysteem;
-   De `Module` op het raadsinformatiesysteem van de documenten;
-   De `Datum` van het hoofddocument op de pagina van het raadsinformatiesysteem.

Als je op de verbinding tussen twee knopen met je muis blijft stilstaan krijg je te zien hoe vaak vanuit het ene document naar het andere document wordt verwezen.

In de informatieblokken hieronder staat hoeveel documenten en hoeveel verbindingen er tussen documenten er in dit netwerk aanwezig zijn.

### Aantal documenten

```{r aantal_nodes}
valueBox(aantal_nodes, icon = "ion-link")
```

### Aantal verbindingen

```{r aantal_edges}
valueBox(aantal_edges, icon = "ion-link")
```

# Overzicht documenten

```{r include=FALSE}
ris_data <- ris_data %>%
  mutate(datum_ris = case_when(module == "Moties, amendemeneten en initiatieven" ~ datum_ingediend, 
                               module == "Raadsvragen" ~ datum_vraag,
                               TRUE ~ datum)
         )

documenten <- tibble(id = netwerk$x$nodes$id) %>% 
  left_join(ris_data %>% select(RIS = ris_nummer, Datum = datum_ris, Module = module, Titel = titel, URL = url_pagina), by = c("id" = "RIS")) %>% 
  rename(RIS = id) %>% 
  distinct() %>% 
  mutate(RIS = glue('<a href="{URL}" target="_blank">{RIS}</a>')) %>% 
  select(-URL)

# documenten <- SharedData$new(documenten)
```

```{r overzicht_documenten}
documenten %>% 
  reactable::reactable(filterable = T, 
                       searchable = T,
                       highlight = T,
                       # striped = T,
                       # theme = reactableTheme(
                       #   borderColor = "#dfe2e5",
                       #   stripedColor = "#f6f8fa",
                       #   highlightColor = "#f0f5f9",
                       #   cellPadding = "8px 12px",
                       #   style = list(fontFamily = "-apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif"),
                       #   searchInputStyle = list(width = "100%")
                       #   ),
                       columns = list(
                         RIS = colDef(name = "RIS-nummer", 
                                      html = T,
                                      sortable = T),
                                      # cell = function(value, index) {
                                      #   # Render als URL
                                      #   sprintf('<a href="%s" target="_blank">%s</a>', documenten$URL[index], value)
                                      #   }),
                         Titel = colDef(name = "Titel"),
                         Datum = colDef(name = "Datum"),
                         Module = colDef(name = "Module")
))
```
